﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanCoinTest.DAL.Models
{
    public class User : BaseEntity
    {
        public string UserName { get; set; }

        public virtual IList<Stake> Stakes { get; set; } = new List<Stake>();

        public virtual IList<QuizGame> QuizGames { get; set; } = new List<QuizGame>();
    }
}
