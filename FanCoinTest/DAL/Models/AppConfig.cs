﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FanCoinTest.DAL.Models
{
    public class AppConfig : BaseEntity { 

        public string ConfigKey { get; set; }

        public string ConfigValue { get; set; }
    }
}
