﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FanCoinTest.DAL.Models
{
    public class Stake: BaseEntity
    {
        public Guid UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public DateTimeOffset? CreatedDateTime { get; set; }

        public bool? IsFinished { get; set; }

        public DateTimeOffset? FinishedDateTime { get; set; }

        public bool? RewardCollected { get; set; }

        public Guid CurrentStakeMillestoneId { get; set; }

        [ForeignKey("CurrentStakeMillestoneId")]
        public virtual StakeMillestone CurrentStakeMillestone { get; set; }

        public int CurrentStakeLength { get; set; }

        public bool IsLatest { get; set; }

        public bool CanCollectReward { get; set; }

        public int StakeLength { get; set; }

        public virtual IList<QuizGame> QuizGames { get; set; } = new List<QuizGame>();
    }
}
