﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FanCoinTest.DAL.Models
{
    public class Fixture : BaseEntity
    {
        [Required]
        public DateTimeOffset? MatchDateTime { get; set; }

        [Required]
        public string AwayTeam { get; set; }

        [Required]
        public string HomeTeam { get; set; }

        [Required]
        public int CompetitionId { get; set; }

        public virtual IEnumerable<QuizGame> QuizGames { get; set; } = new List<QuizGame>();
    }
}
