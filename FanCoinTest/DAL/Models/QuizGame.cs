﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FanCoinTest.DAL.Models
{
    public class QuizGame: BaseEntity 
    {
        public Guid UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public float? GameSize { get; set; }

        public float? PickCount { get; set; }

        public float? CorrectAnswersCount { get; set; }

        public float? Score { get; set; }

        public Guid UId { get; set; }

        [ForeignKey("UId")]
        public virtual Fixture Fixture { get; set; }

        public float? OptaFixtureId { get; set; }

        public bool? IsGameFinished { get; set; }

        public DateTimeOffset? SubmitDateTime { get; set; }

        public Guid? StakeId { get; set; }

        [ForeignKey("StakeId")]
        public virtual Stake Stake { get; set; }

    }
}
