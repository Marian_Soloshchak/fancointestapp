﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanCoinTest.DAL.Models
{
    public class StakeMillestone: BaseEntity
    {
        public int GamesCount { get; set; }

        public int Reward { get; set; }

        public virtual IList<Stake> Stakes { get; set; } = new List<Stake>();
    }
}
