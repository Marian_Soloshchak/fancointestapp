﻿using FanCoinTest.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace FanCoinTest.DAL
{
    public class FanCoinContext : DbContext
    {

        public DbSet<Fixture> Fixtures { get; set; }
        public DbSet<QuizGame> QuizGames { get; set; }
        public DbSet<AppConfig> AppConfig { get; set; }
        public DbSet<StakeMillestone> StakeMillestones { get; set; }
        public DbSet<Stake> Stakes { get; set; }


        public FanCoinContext(DbContextOptions<FanCoinContext> options)
        : base(options)
        {
            Database.Migrate();
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<QuizGame>(quizGame =>
            {
                quizGame.HasOne(q => q.Fixture)
                        .WithMany(f => f.QuizGames)
                        .HasForeignKey(q => q.UId)
                        .OnDelete(DeleteBehavior.Restrict);
                quizGame.HasOne(q => q.User)
                        .WithMany(f => f.QuizGames)
                        .HasForeignKey(q => q.UserId)
                        .OnDelete(DeleteBehavior.Restrict);
                quizGame.HasOne(q => q.Stake)
                        .WithMany(f => f.QuizGames)
                        .HasForeignKey(q => q.StakeId)
                        .IsRequired(false)
                        .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Stake>(stake =>
            {
                stake.HasOne(s => s.CurrentStakeMillestone)
                     .WithMany(m => m.Stakes)
                     .HasForeignKey(s => s.CurrentStakeMillestoneId)
                     .OnDelete(DeleteBehavior.Restrict);
                stake.HasOne(s => s.User)
                     .WithMany(u => u.Stakes)
                     .HasForeignKey(s => s.UserId)
                     .OnDelete(DeleteBehavior.Restrict);
            });

            this.SeedDatabase(modelBuilder);
        }

        private void SeedDatabase(ModelBuilder modelBuilder)
        {
            var user = new User()
            {
                Id = Guid.NewGuid(),
                UserName = "TestUser"
            };
            modelBuilder.Entity<User>().HasData(user);

            modelBuilder.Entity<AppConfig>().HasData(new AppConfig()
            {
                Id = Guid.NewGuid(),
                ConfigKey = "stake_length",
                ConfigValue = "20"
            });

            var stakeMilllestones = new List<StakeMillestone>()
            {
                new StakeMillestone()
                {
                    Id = Guid.NewGuid(),
                    GamesCount = 5,
                    Reward = 50
                },
                new StakeMillestone()
                {
                    Id = Guid.NewGuid(),
                    GamesCount = 10,
                    Reward = 100
                },
                new StakeMillestone()
                {
                    Id = Guid.NewGuid(),
                    GamesCount = 15,
                    Reward = 200
                },
                new StakeMillestone()
                {
                    Id = Guid.NewGuid(),
                    GamesCount = 20,
                    Reward = 500
                }
            };
            modelBuilder.Entity<StakeMillestone>().HasData(stakeMilllestones);
            
        }
    }
}
