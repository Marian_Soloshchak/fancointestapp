﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanCoinTest.DAL;
using FanCoinTest.DAL.Models;
using FanCoinTest.Services;
using FanCoinTest.Services.Abstract;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AutoMapper;
using FanCoinTest.Models.ViewModels;

namespace FanCoinTest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Add CORS

            services.AddCors(options => options.AddPolicy("Cors", builder =>
            {
                builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader();
            }));
            #endregion

            #region Add Entity Framework

            services.AddDbContext<FanCoinContext>(options =>
                   options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"))
               );

            #endregion

            services.AddTransient<IBaseService<Fixture>, FixturesService>();
            services.AddTransient<IBaseService<QuizGame>, QuizGamesService>();
            services.AddTransient<IBaseService<User>, UsersService>();
            services.AddTransient<IBaseService<AppConfig>, AppConfigService>();
            services.AddTransient<IBaseService<StakeMillestone>, StakeMillestonesService>();
            services.AddTransient<IStakesService, StakesService>();

            
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options => {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("Cors");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
