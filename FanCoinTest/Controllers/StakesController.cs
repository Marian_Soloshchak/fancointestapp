﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FanCoinTest.DAL.Models;
using FanCoinTest.Models.ViewModels;
using FanCoinTest.Services.Abstract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FanCoinTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StakesController : ControllerBase
    {
        private readonly IStakesService _stakesService;

        private readonly IBaseService<User> _usersService;

        private readonly IMapper _mapper;

        public StakesController(IStakesService stakesService, IBaseService<User> usersService)
        {
            _stakesService = stakesService;
            _usersService = usersService;

            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Fixture, FixtureViewModel>();
                cfg.CreateMap<QuizGame, QuizGameViewModel>();
                cfg.CreateMap<Stake, StakeViewModel>();
                cfg.CreateMap<StakeMillestone, StakeMillestoneViewModel>();
            });
            // only during development, validate your mappings; remove it before release
            configuration.AssertConfigurationIsValid();

            _mapper = configuration.CreateMapper();
        }
        // GET: api/Stakes
        [HttpGet]
        public async Task <IEnumerable<StakeViewModel>> Get()
        {
            return (await _stakesService.GetAsync()).Select( stake => _mapper.Map<StakeViewModel>(stake));
        }

        // GET: api/Stakes/{iod}
        [HttpGet("{id}")]
        public async Task<Stake> Get(string id)
        {
            return await _stakesService.GetById(id);
        }

        // GET: api/Stakes/user/{id}
        [HttpGet("user/{id}")]
        public async Task<IEnumerable<StakeViewModel>> GetByUserId(string id)
        {
            //todo
            return (await _stakesService.GetAsync(id)).Select(stake => _mapper.Map<StakeViewModel>(stake));
        }

        // It's just helper. Should be in some usersController
        // GET: api/Stakes/getUser
        [HttpGet("getUser")]
        public async Task<User> GetCurrentUser()
        {
            var user =  (await _usersService.GetAsync()).ToList().FirstOrDefault();
            return user;
        }

        // GET: api/Stakes/{iod}
        [HttpGet("collectReward/{id}")]
        public async Task<Stake> CollectReward(string id)
        {
            return await _stakesService.CollectReward(id);
        }

        // POST: api/Stakes
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Stakes/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<Stake> Delete(string id)
        {
            return await _stakesService.Remove(id);
        }
    }
}
