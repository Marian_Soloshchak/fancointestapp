﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FanCoinTest.DAL.Models;
using FanCoinTest.Models;
using FanCoinTest.Models.ViewModels;
using FanCoinTest.Services.Abstract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FanCoinTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuizGamesController : ControllerBase
    {
        private readonly IBaseService<QuizGame> _quizGamesService;

        private readonly IStakesService _stakesService;

        private readonly IMapper _mapper;


        public QuizGamesController(IBaseService<QuizGame> quizGamesService, IStakesService stakesService)
        {
            _quizGamesService = quizGamesService;
            _stakesService = stakesService;
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Fixture, FixtureViewModel>();
                cfg.CreateMap<QuizGame, QuizGameViewModel>();
                cfg.CreateMap<Stake, StakeViewModel>();
                cfg.CreateMap<StakeMillestone, StakeMillestoneViewModel>();
            });
            // only during development, validate your mappings; remove it before release
            configuration.AssertConfigurationIsValid();

            _mapper = configuration.CreateMapper();
        }
        // GET: api/QuizGames
        [HttpGet]
        public async Task<IEnumerable<QuizGameViewModel>> Get()
        {
            return (await _quizGamesService.GetAsync()).Select( game => _mapper.Map<QuizGameViewModel>(game));
        }

        // GET: api/QuizGames/{id}
        [HttpGet("{id}", Name = "Get")]
        public async Task<QuizGame> Get(string id)
        {
            return await this._quizGamesService.GetById(id);
        }

        // POST: api/QuizGames
        [HttpPost]
        public async Task<StakeViewModel> Post([FromBody] AddQuizGame request)
        {
            var quizGame = new QuizGame()
            {
                CorrectAnswersCount = request.CorrectAnswersCount,
                GameSize = request.GameSize,
                IsGameFinished = request.IsGameFinished,
                OptaFixtureId = 0,
                PickCount = request.PickCount,
                Score = request.Score,
                SubmitDateTime = new DateTimeOffset(DateTime.Now),
                UserId = new Guid(request.UserId),
                UId = new Guid(request.UId),
                StakeId = null
            };
            quizGame = await this._quizGamesService.Add(quizGame);
            var stake  = await this._stakesService.AssingQuizToStake(quizGame);
            return _mapper.Map<StakeViewModel>(stake);
        }

        // PUT: api/QuizGames
        [HttpPut]
        public async Task<StakeViewModel> Put([FromBody] QuizGame entry)
        {
            if(entry.CorrectAnswersCount.HasValue && entry.CorrectAnswersCount.Value >=0)
            {
                entry.IsGameFinished = true;
            }
            var quizGame = await _quizGamesService.Update(entry);
            var stake = await _stakesService.UpdateStakeAfterQuizGameUpdate(quizGame.StakeId.ToString());
            return _mapper.Map<StakeViewModel>(stake);
        }
        
    }
}
