﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanCoinTest.DAL.Models;
using FanCoinTest.Services.Abstract;
using Microsoft.AspNetCore.Mvc;

namespace FanCoinTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FixturesController : ControllerBase
    {
        private IBaseService<Fixture> _fixturesService { get; set; }

        public FixturesController(IBaseService<Fixture> fixturesService)
        {
            this._fixturesService = fixturesService;
        }

        [HttpGet]
        public async Task<IEnumerable<Fixture>> Get()
        {
            return await this._fixturesService.GetAsync();
        }
        
        [HttpGet("{id}")]
        public async Task<Fixture> Get(string id)
        {
            return await this._fixturesService.GetById(id);
        }
        
        [HttpPost]
        public async Task<Fixture> Post([FromBody]Fixture entry)
        {
            return await this._fixturesService.Add(entry);
        }
    }
}
