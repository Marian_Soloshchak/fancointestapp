﻿using FanCoinTest.DAL;
using FanCoinTest.DAL.Models;
using FanCoinTest.Services.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FanCoinTest.Services
{
    public class StakesService: IStakesService, IDisposable
    {
        private readonly FanCoinContext _context;

        private readonly DbSet<Stake> _entities;

        private readonly IBaseService<AppConfig> _appConfigService;

        private readonly IBaseService<StakeMillestone> _stakeMillestonesService;

        public StakesService(FanCoinContext context, IBaseService<AppConfig> appConfigService, IBaseService<StakeMillestone> stakeMillestonesService)
        {
            _context = context;
            _entities = context.Set<Stake>();
            _appConfigService = appConfigService;
            _stakeMillestonesService = stakeMillestonesService;
        }

        public async Task<IEnumerable<Stake>> GetAsync()
        {
            return await this._entities
                .Include(e => e.QuizGames)
                .ThenInclude(q => q.Fixture)
                .Include(e => e.CurrentStakeMillestone)
                .ToListAsync();
        }

        public async Task<IEnumerable<Stake>> GetAsync(string userId)
        {
            var guid = new Guid(userId);
            return await this._entities
                .Where( e => e.UserId == guid)
                .Include(e => e.QuizGames)
                .ThenInclude(q => q.Fixture)
                .Include(e => e.CurrentStakeMillestone)
                //.Include(e => e.User)
                .ToListAsync();
        }

        public async Task<Stake> GetById(string id)
        {
            var guid = new Guid(id);
            return await this._entities
                .Where(e => e.Id == guid)
                //.Include(e => e.User)
                .Include(e => e.QuizGames)
                .Include(e => e.CurrentStakeMillestone)
                .FirstOrDefaultAsync();
        }

        public async Task<Stake> Add(Stake entry)
        {
            if (entry == null)
                throw new ArgumentNullException("Input data is null");
            var newEntity = await _entities.AddAsync(entry);
            return newEntity.Entity;
        }

        public Stake Update(Stake stake)
        {
            if (stake == null)
                throw new ArgumentNullException("Input data is null");
            var newEntity = _entities.Update(stake);
            return newEntity.Entity;
        }

        public async Task<Stake> CollectReward(string stakeId)
        {
            var stake = await this.GetById(stakeId);
            if (!stake.CanCollectReward 
                || (stake.RewardCollected.HasValue && stake.RewardCollected.Value))
            {
                return null;
            }
            stake.RewardCollected = true;
            stake.IsFinished = true;
            stake.CanCollectReward = false;
            stake.FinishedDateTime = new DateTimeOffset(DateTime.Now);
            this.Update(stake);
            return await this.GetById(stake.Id.ToString());
        }

        public async Task<Stake> UpdateStakeAfterQuizGameUpdate(string stakeId)
        {
            var stake = await this.GetById(stakeId);
            stake.QuizGames = stake.QuizGames.OrderBy(q => q.SubmitDateTime).ToList();
            var counter = 0;
            int? indexOfFirstUnPassed = null;
            for (int i = 0; i < stake.QuizGames.Count; i++)
            {
                if(stake.QuizGames[i].CorrectAnswersCount == -1 || stake.QuizGames[i].CorrectAnswersCount >= 8)
                {
                    if(stake.QuizGames[i].CorrectAnswersCount >= 8)
                    {
                        counter++;
                    }
                } else
                {
                    indexOfFirstUnPassed = i;
                    break;
                }
            }
            stake.CurrentStakeLength = counter;
            if (indexOfFirstUnPassed.HasValue)
            {
                stake.CanCollectReward = false;
                stake.IsFinished = true;
                stake.IsLatest = false;
                stake.FinishedDateTime = new DateTimeOffset(DateTime.Now);
                var quizGamesToTransfer = stake.QuizGames.TakeLast(stake.QuizGames.Count - indexOfFirstUnPassed.Value - 1);
                Update(stake);
                foreach (var item in quizGamesToTransfer)
                {
                    stake = await AssingQuizToStake(item);
                }
            } else
            {
                var stakeMillestone = (await _stakeMillestonesService.GetAsync()).OrderBy(s => s.GamesCount).Where(s => s.GamesCount >= stake.QuizGames.Count).FirstOrDefault();
                if (stakeMillestone != null)
                {
                    stake.CurrentStakeMillestoneId = stakeMillestone.Id;
                }
                stake.CanCollectReward = stake.CurrentStakeLength == stakeMillestone.GamesCount;
                if (stake.CurrentStakeLength == stake.StakeLength)
                {
                    stake.IsFinished = true;
                }
                Update(stake);
            }
            return stake;
        }

        public async Task<Stake> Remove(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("Input data is null");
            }
            var entityToRemove = await this.GetById(id);
            var deletedEntity = _entities.Remove(entityToRemove);
            return deletedEntity.Entity;
        }

        public async Task<IEnumerable<Stake>> WhereAsync(Expression<Func<Stake, bool>> exp)
        {
            return await this._entities
                 .Include( e => e.CurrentStakeMillestone)
                 .Where(exp)
                 .ToListAsync();
        }

        public async Task<Stake> CreateNewForUser(string userId)
        {
            var stakeConfig = (await this._appConfigService.WhereAsync(x => x.ConfigKey == "stake_length")).FirstOrDefault();
            if (stakeConfig == null)
                throw new KeyNotFoundException("Couldn't find stake_length config");
            int.TryParse(stakeConfig.ConfigValue, out int stakeLength);

            var minStakeMillestone = (await _stakeMillestonesService.GetAsync()).OrderBy(s => s.GamesCount).First();
            var stake = new Stake()
            {
                UserId = new Guid(userId),
                StakeLength = stakeLength,
                CurrentStakeLength = 0,
                IsLatest = true,
                IsFinished = false,
                CreatedDateTime = new DateTimeOffset(DateTime.Now),
                RewardCollected = false,
                CanCollectReward = false,
                FinishedDateTime = null,
                CurrentStakeMillestoneId = minStakeMillestone.Id
            };
            stake = await Add(stake);
            _context.SaveChanges();
            return stake;
        }

        public async Task<Stake> AssingQuizToStake(QuizGame quizGame)
        {
            var currentStake = await this.GetLatestStakeForUser(quizGame.UserId.ToString());
            if (currentStake == null)
            {
                currentStake = await CreateNewForUser(quizGame.UserId.ToString());
            }

            if ((currentStake.CurrentStakeLength == currentStake.StakeLength) || (currentStake.IsFinished.HasValue && currentStake.IsFinished.Value))
            {
                currentStake.IsLatest = false;
                Update(currentStake);
                currentStake = await CreateNewForUser(quizGame.UserId.ToString());
            }
            if (currentStake.QuizGames == null)
            {
                currentStake.QuizGames = new List<QuizGame>();
            }
            currentStake.QuizGames.Add(quizGame);
            var counter = 0;
            for (int i = 0; i < currentStake.QuizGames.Count; i++)
            {
                if (currentStake.QuizGames[i].CorrectAnswersCount >= 8)
                {
                    counter++;
                }
            }
            currentStake.CurrentStakeLength = counter;

            var stakeMillestone = (await _stakeMillestonesService.GetAsync()).OrderBy(s => s.GamesCount).Where(s => s.GamesCount >= currentStake.QuizGames.Count).FirstOrDefault();
            if (stakeMillestone != null)
            {
                currentStake.CurrentStakeMillestoneId = stakeMillestone.Id;
            }
            if (currentStake.CurrentStakeLength == currentStake.StakeLength)
            {
                currentStake.IsFinished = true;
            }
            currentStake = Update(currentStake);
            return currentStake;
        }

        public async Task<Stake> GetLatestStakeForUser(string userId)
        {
            var id = new Guid(userId);
            var latestStake = await this._entities
                .Where( e => e.IsLatest)
                .Include(e => e.QuizGames)
                //.Include(e => e.CurrentStakeMillestone)
                //.Include(e => e.User)
                .FirstOrDefaultAsync();
            return latestStake;
        }

        public void Dispose()
        {
            _context.SaveChanges();
        }
    }
}
