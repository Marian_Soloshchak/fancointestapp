﻿using FanCoinTest.DAL;
using FanCoinTest.DAL.Models;
using FanCoinTest.Services.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FanCoinTest.Services
{
    public class QuizGamesService : IBaseService<QuizGame>, IDisposable
    {

        private readonly FanCoinContext _context;

        private readonly DbSet<QuizGame> _entities;

        public QuizGamesService(FanCoinContext context)
        {
            _context = context;
            _entities = context.Set<QuizGame>();
        }

        public async Task<IEnumerable<QuizGame>> GetAsync()
        {
            return await this._entities
              //.Include(e => e.Fixture)
              .ToListAsync();
        }

        public async Task<QuizGame> GetById(string id)
        {
            var guid = new Guid(id);
            return await this._entities
                .Where(e => e.Id == guid)
                //.Include(e => e.Fixture)
                .FirstOrDefaultAsync();
        }

        public async Task<QuizGame> Add(QuizGame entry)
        {
            if (entry == null)
                throw new ArgumentNullException("Input data is null");
            var newEntity = await _entities.AddAsync(entry);
            return newEntity.Entity;
        }

        public async Task<QuizGame> Update(QuizGame entry)
        {
            if (entry == null)
                throw new ArgumentNullException("Input data is null");
            var newEntity = _entities.Update(entry);
            return newEntity.Entity;
        }

        public async Task<QuizGame> Remove(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("Input data is null");
            }
            var entityToRemove = await this.GetById(id);
            var deletedEntity = _entities.Remove(entityToRemove);
            return deletedEntity.Entity;
        }

        public async Task<IEnumerable<QuizGame>> WhereAsync(Expression<Func<QuizGame, bool>> exp)
        {
            return await this._entities
                 //.Include(e => e.Fixture)
                 .Where(exp)
                 .ToListAsync();
        }
        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.SaveChanges();
        }
    }
}
