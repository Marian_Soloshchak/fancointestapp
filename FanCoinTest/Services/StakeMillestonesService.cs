﻿using FanCoinTest.DAL;
using FanCoinTest.DAL.Models;
using FanCoinTest.Services.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FanCoinTest.Services
{
    public class StakeMillestonesService: IBaseService<StakeMillestone>, IDisposable
    {
        private readonly FanCoinContext _context;

        private readonly DbSet<StakeMillestone> _entities;

        public StakeMillestonesService(FanCoinContext context)
        {
            _context = context;
            _entities = context.Set<StakeMillestone>();
        }

        public async Task<IEnumerable<StakeMillestone>> GetAsync()
        {
            return await this._entities
              .ToListAsync();
        }

        public async Task<StakeMillestone> GetById(string id)
        {
            var guid = new Guid(id);
            return await this._entities
                .Where(e => e.Id == guid)
                .FirstOrDefaultAsync();
        }

        public async Task<StakeMillestone> Add(StakeMillestone entry)
        {
            if (entry == null)
                throw new ArgumentNullException("Input data is null");
            var newEntity = await _entities.AddAsync(entry);
            return newEntity.Entity;
        }

        public async Task<StakeMillestone> Update(StakeMillestone entry)
        {
            if (entry == null)
                throw new ArgumentNullException("Input data is null");
            var newEntity = _entities.Update(entry);
            return newEntity.Entity;
        }

        public async Task<StakeMillestone> Remove(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("Input data is null");
            }
            var entityToRemove = await this.GetById(id);
            var deletedEntity = _entities.Remove(entityToRemove);
            return deletedEntity.Entity;
        }

        public async Task<IEnumerable<StakeMillestone>> WhereAsync(Expression<Func<StakeMillestone, bool>> exp)
        {
            return await this._entities
                 .Where(exp)
                 .ToListAsync();
        }
        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.SaveChanges();
        }
    }
}
