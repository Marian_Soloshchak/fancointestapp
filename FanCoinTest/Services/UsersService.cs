﻿using FanCoinTest.DAL;
using FanCoinTest.DAL.Models;
using FanCoinTest.Services.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FanCoinTest.Services
{
    public class UsersService: IBaseService<User>, IDisposable
    {
        private readonly FanCoinContext _context;

        private readonly DbSet<User> _entities;

        public UsersService(FanCoinContext context)
        {
            _context = context;
            _entities = context.Set<User>();
        }

        public async Task<IEnumerable<User>> GetAsync()
        {
            return await this._entities
              .ToListAsync();
        }

        public async Task<User> GetById(string id)
        {
            var guid = new Guid(id);
            return await this._entities
                .Where(e => e.Id == guid)
                .FirstOrDefaultAsync();
        }

        public async Task<User> Add(User entry)
        {
            if (entry == null)
                throw new ArgumentNullException("Input data is null");
            var newEntity = await _entities.AddAsync(entry);
            return newEntity.Entity;
        }

        public async Task<User> Update(User entry)
        {
            if (entry == null)
                throw new ArgumentNullException("Input data is null");
            var newEntity = _entities.Update(entry);
            return newEntity.Entity;
        }

        public async Task<User> Remove(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("Input data is null");
            }
            var entityToRemove = await this.GetById(id);
            var deletedEntity = _entities.Remove(entityToRemove);
            return deletedEntity.Entity;
        }

        public async Task<IEnumerable<User>> WhereAsync(Expression<Func<User, bool>> exp)
        {
            return await this._entities
                 .Where(exp)
                 .ToListAsync();
        }
        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.SaveChanges();
        }
    }
}
