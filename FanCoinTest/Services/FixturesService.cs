﻿using FanCoinTest.DAL;
using FanCoinTest.DAL.Models;
using FanCoinTest.Services.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FanCoinTest.Services
{
    public class FixturesService : IBaseService<Fixture>, IDisposable
    {
        private readonly FanCoinContext _context;

        private readonly DbSet<Fixture> _entities;

        public FixturesService(FanCoinContext context)
        {
            _context = context;
            _entities = context.Set<Fixture>();
        }

        public async Task<IEnumerable<Fixture>> GetAsync()
        {
            return await this._entities
                .Include(e => e.QuizGames)
                .ToListAsync();
        }

        public async Task<Fixture> GetById(string id)
        {
            var guid = new Guid(id);
            return await this._entities
                .Where(e => e.Id == guid)
                .Include(e => e.QuizGames)
                .FirstOrDefaultAsync();
        }

        public async Task<Fixture> Add(Fixture entry)
        {
            if (entry == null)
                throw new ArgumentNullException("Input data is null");
            var newEntity = await _entities.AddAsync(entry);
            return newEntity.Entity;
        }

        public async Task<Fixture> Update(Fixture entry)
        {
            if (entry == null)
                throw new ArgumentNullException("Input data is null");
            var newEntity = _entities.Update(entry);
            return newEntity.Entity;
        }

        public async Task<Fixture> Remove(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("Input data is null");
            }
            var entityToRemove = await this.GetById(id);
            var deletedEntity = _entities.Remove(entityToRemove);
            return deletedEntity.Entity;
        }

        public async Task<IEnumerable<Fixture>> WhereAsync(Expression<Func<Fixture, bool>> exp)
        {
            return await this._entities
                .Include(e => e.QuizGames)
                .Where(exp)
                .ToListAsync();
        }
        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.SaveChanges();
        }
    }
}
