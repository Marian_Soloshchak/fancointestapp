﻿using FanCoinTest.DAL;
using FanCoinTest.DAL.Models;
using FanCoinTest.Services.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FanCoinTest.Services
{
    public class AppConfigService: IBaseService<AppConfig>, IDisposable
    {
        private readonly FanCoinContext _context;

        private readonly DbSet<AppConfig> _entities;

        public AppConfigService(FanCoinContext context)
        {
            _context = context;
            _entities = context.Set<AppConfig>();
        }

        public async Task<IEnumerable<AppConfig>> GetAsync()
        {
            return await this._entities
              .ToListAsync();
        }

        public async Task<AppConfig> GetById(string id)
        {
            var guid = new Guid(id);
            return await this._entities
                .Where(e => e.Id == guid)
                .FirstOrDefaultAsync();
        }

        public async Task<AppConfig> Add(AppConfig entry)
        {
            if (entry == null)
                throw new ArgumentNullException("Input data is null");
            var newEntity = await _entities.AddAsync(entry);
            return newEntity.Entity;
        }

        public async Task<AppConfig> Update(AppConfig entry)
        {
            if (entry == null)
                throw new ArgumentNullException("Input data is null");
            var newEntity = _entities.Update(entry);
            return newEntity.Entity;
        }

        public async Task<AppConfig> Remove(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("Input data is null");
            }
            var entityToRemove = await this.GetById(id);
            var deletedEntity = _entities.Remove(entityToRemove);
            return deletedEntity.Entity;
        }

        public async Task<IEnumerable<AppConfig>> WhereAsync(Expression<Func<AppConfig, bool>> exp)
        {
            return await this._entities
                 .Where(exp)
                 .ToListAsync();
        }
        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.SaveChanges();
        }
    }
}
