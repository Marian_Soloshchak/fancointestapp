﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FanCoinTest.Services.Abstract
{
    public interface IBaseService<T> where T : class
    {

        Task<IEnumerable<T>> GetAsync();

        Task<T> GetById(string id);

        Task<IEnumerable<T>> WhereAsync(Expression<Func<T, bool>> exp);

        Task<T> Add(T entry);

        Task<T> Update(T entry);

        Task<T> Remove(string id);
        void SaveChanges();
    }
}
