﻿using FanCoinTest.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FanCoinTest.Services.Abstract
{
    public interface IStakesService
    {
        Task<IEnumerable<Stake>> GetAsync();

        Task<IEnumerable<Stake>> GetAsync(string userId);

        Task<Stake> GetById(string id);

        Task<IEnumerable<Stake>> WhereAsync(Expression<Func<Stake, bool>> exp);

        Task<Stake> Add(Stake entry);

        Task<Stake> CollectReward(string stakeId);

        Task<Stake> CreateNewForUser(string userId);

        Task<Stake> AssingQuizToStake(QuizGame quizGame);

        Task<Stake> UpdateStakeAfterQuizGameUpdate(string stakeId);

        Stake Update(Stake stake);

        Task<Stake> Remove(string id);

        Task<Stake> GetLatestStakeForUser(string userId);
    }

}
