﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanCoinTest.Migrations
{
    public partial class Seeddb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppConfig",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ConfigKey = table.Column<string>(nullable: true),
                    ConfigValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppConfig", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Fixtures",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    MatchDateTime = table.Column<DateTimeOffset>(nullable: false),
                    AwayTeam = table.Column<string>(nullable: false),
                    HomeTeam = table.Column<string>(nullable: false),
                    CompetitionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fixtures", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StakeMillestones",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GamesCount = table.Column<int>(nullable: false),
                    Reward = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StakeMillestones", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Stakes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    CreatedDateTime = table.Column<DateTimeOffset>(nullable: true),
                    IsFinished = table.Column<bool>(nullable: true),
                    FinishedDateTime = table.Column<DateTimeOffset>(nullable: true),
                    RewardCollected = table.Column<bool>(nullable: true),
                    CurrentStakeMillestoneId = table.Column<Guid>(nullable: false),
                    CurrentStakeLength = table.Column<int>(nullable: false),
                    IsLatest = table.Column<bool>(nullable: false),
                    CanCollectReward = table.Column<bool>(nullable: false),
                    StakeLength = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stakes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Stakes_StakeMillestones_CurrentStakeMillestoneId",
                        column: x => x.CurrentStakeMillestoneId,
                        principalTable: "StakeMillestones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Stakes_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QuizGames",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    GameSize = table.Column<float>(nullable: true),
                    PickCount = table.Column<float>(nullable: true),
                    CorrectAnswersCount = table.Column<float>(nullable: true),
                    Score = table.Column<float>(nullable: true),
                    UId = table.Column<Guid>(nullable: false),
                    OptaFixtureId = table.Column<float>(nullable: true),
                    IsGameFinished = table.Column<bool>(nullable: true),
                    SubmitDateTime = table.Column<DateTimeOffset>(nullable: true),
                    StakeId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuizGames", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuizGames_Stakes_StakeId",
                        column: x => x.StakeId,
                        principalTable: "Stakes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QuizGames_Fixtures_UId",
                        column: x => x.UId,
                        principalTable: "Fixtures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_QuizGames_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AppConfig",
                columns: new[] { "Id", "ConfigKey", "ConfigValue" },
                values: new object[] { new Guid("b4acf0b6-4d97-4ec8-95fb-36c1fc51aebc"), "stake_length", "20" });

            migrationBuilder.InsertData(
                table: "StakeMillestones",
                columns: new[] { "Id", "GamesCount", "Reward" },
                values: new object[,]
                {
                    { new Guid("9ba8546a-1bc2-4d55-8ad2-18f8984e3576"), 5, 50 },
                    { new Guid("9f33e54c-c2f2-4a25-96fe-49ad975fc681"), 10, 100 },
                    { new Guid("caa9da46-66fc-4b85-9b7e-080072b8a43a"), 15, 200 },
                    { new Guid("2eb2de53-5653-4e59-a86d-c3908b96a9a4"), 20, 500 }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "UserName" },
                values: new object[] { new Guid("2a32927a-565f-465a-b864-75c4272587a0"), "TestUser" });

            migrationBuilder.CreateIndex(
                name: "IX_QuizGames_StakeId",
                table: "QuizGames",
                column: "StakeId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizGames_UId",
                table: "QuizGames",
                column: "UId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizGames_UserId",
                table: "QuizGames",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Stakes_CurrentStakeMillestoneId",
                table: "Stakes",
                column: "CurrentStakeMillestoneId");

            migrationBuilder.CreateIndex(
                name: "IX_Stakes_UserId",
                table: "Stakes",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppConfig");

            migrationBuilder.DropTable(
                name: "QuizGames");

            migrationBuilder.DropTable(
                name: "Stakes");

            migrationBuilder.DropTable(
                name: "Fixtures");

            migrationBuilder.DropTable(
                name: "StakeMillestones");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
