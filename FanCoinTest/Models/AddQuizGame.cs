﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanCoinTest.Models
{
    public class AddQuizGame
    {
        public string UserId { get; set; }

        public float? GameSize { get; set; }

        public float? PickCount { get; set; }

        public float? CorrectAnswersCount { get; set; }

        public float? Score { get; set; }

        public string UId { get; set; }

        public float? OptaFixtureId { get; set; }

        public bool? IsGameFinished { get; set; }
    }
}
