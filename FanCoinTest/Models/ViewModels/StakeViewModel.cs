﻿using FanCoinTest.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanCoinTest.Models.ViewModels
{
    public class StakeViewModel
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public DateTimeOffset? CreatedDateTime { get; set; }

        public bool? IsFinished { get; set; }

        public DateTimeOffset? FinishedDateTime { get; set; }

        public bool? RewardCollected { get; set; }

        public Guid CurrentStakeMillestoneId { get; set; }

        public StakeMillestoneViewModel CurrentStakeMillestone { get; set; }

        public int CurrentStakeLength { get; set; }

        public bool IsLatest { get; set; }

        public bool CanCollectReward { get; set; }

        public int StakeLength { get; set; }

        public IList<QuizGameViewModel> QuizGames { get; set; } = new List<QuizGameViewModel>();
    }
}
