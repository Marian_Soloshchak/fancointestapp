﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanCoinTest.Models.ViewModels
{
    public class StakeMillestoneViewModel
    {
        public Guid Id { get; set; }

        public int GamesCount { get; set; }

        public int Reward { get; set; }
    }
}
