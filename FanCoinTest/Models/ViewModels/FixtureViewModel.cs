﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanCoinTest.Models.ViewModels
{
    public class FixtureViewModel
    {
        public DateTimeOffset? MatchDateTime { get; set; }
        
        public string AwayTeam { get; set; }
        
        public string HomeTeam { get; set; }
        
        public int CompetitionId { get; set; }
    }
}
