﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanCoinTest.Models.ViewModels
{
    public class QuizGameViewModel
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public float? GameSize { get; set; }

        public float? PickCount { get; set; }

        public float? CorrectAnswersCount { get; set; }

        public float? Score { get; set; }

        public Guid UId { get; set; }

        public FixtureViewModel Fixture { get; set; }

        public float? OptaFixtureId { get; set; }

        public bool? IsGameFinished { get; set; }

        public DateTimeOffset? SubmitDateTime { get; set; }

        public Guid? StakeId { get; set; }
    }
}
