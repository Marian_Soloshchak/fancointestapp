import { Component, ViewChild } from '@angular/core';

import { HomePage } from '../home/home';
import { OffersPage } from '../offers/offers';
import { JackpotPage } from '../jackpot/jackpot';
import { ResultsPage } from '../results/results';
import { ProfilePage } from '../profile/profile';
import { NavController, Tabs } from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tabOffersRoot = OffersPage;
  tabJackpotRoot = JackpotPage;
  tabHomeRoot = HomePage;
  tabResultsRoot = ResultsPage;
  tabProfileRoot = ProfilePage;

  constructor(private nav: NavController) {
  }
}
