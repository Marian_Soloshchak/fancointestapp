import { Component, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StakesProvider } from './services/stakes';
import { Subscription } from 'rxjs/Subscription';


@IonicPage()
@Component({
  selector: 'page-jackpot',
  templateUrl: 'jackpot.html',
})
export class JackpotPage implements OnDestroy {

  private subscriptions: Array<Subscription> = []

  currentUser: any;
  stakes: Array<any>;

  constructor(
    public navCtrl: NavController,
     public navParams: NavParams,
     private stakesService: StakesProvider
     ) {
      this.subscriptions.push(
        this.stakesService.getStakes().subscribe((stakes: Array<any>) => {
          this.stakes = stakes
          console.log(this.stakes);
        })
      )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad JackpotPage');
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

}
