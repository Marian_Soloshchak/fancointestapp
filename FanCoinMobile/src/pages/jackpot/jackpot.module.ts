import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JackpotPage } from './jackpot';
import { StakesProvider } from './services/stakes';
import { StakesListComponent } from './components/stakes-list/stakes-list';
import { StakeListItemComponent } from './components/stake-list-item/stake-list-item';
import { QuizGameComponent } from './components/quiz-game/quiz-game';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    JackpotPage,
    StakesListComponent,
    StakeListItemComponent,
    QuizGameComponent
  ],
  imports: [
    IonicPageModule.forChild(JackpotPage),
    NgCircleProgressModule,
    ComponentsModule
  ],
  entryComponents: [
    JackpotPage,
    StakesListComponent,
    StakeListItemComponent,
    QuizGameComponent
  ],
  providers: [
    StakesProvider
  ],
  exports: [
    StakesListComponent,
    StakeListItemComponent,
    QuizGameComponent
  ]
})
export class JackpotPageModule {}
