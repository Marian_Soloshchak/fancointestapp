import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class StakesProvider {

  private apiUrl = 'https://localhost:44395/api';

  constructor(public http: HttpClient) {
  }

  getStakes() {
    return this.http.get(`${this.apiUrl}/stakes`);
  }

  getUser() {
    return this.http.get(`${this.apiUrl}/stakes/getUser`)
  }

  collectReward(stakeId: string){
    return this.http.get(`${this.apiUrl}/stakes/collectReward/${stakeId}`)
  }

}
