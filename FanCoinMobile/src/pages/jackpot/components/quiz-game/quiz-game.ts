import { Component, Input } from '@angular/core';

/**
 * Generated class for the QuizGameComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'quiz-game',
  templateUrl: 'quiz-game.html'
})
export class QuizGameComponent {

  @Input() quizGame: any;
  @Input() index: number;
  @Input() connectWithNext: boolean;

  homeResult = 0;
  awayResult = 0;
  
  constructor() {

    // generating random match result because don't have this fields in db
    this.homeResult = this.getRandomInt(0, 5);
    this.awayResult = this.getRandomInt(0, 5);
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

}
