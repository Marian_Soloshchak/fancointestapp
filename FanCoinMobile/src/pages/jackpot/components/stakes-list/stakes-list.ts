import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { Slides } from 'ionic-angular';

@Component({
  selector: 'stakes-list',
  templateUrl: 'stakes-list.html'
})
export class StakesListComponent{

  @ViewChild('slides') slides: Slides;
  @Input() stakes: Array<any>;
  currentIndex;

  constructor() {
  }

  slideChanged(){
    this.currentIndex = this.slides.getActiveIndex();
    if (this.slides.getActiveIndex() == this.stakes.length) {
      this.currentIndex = this.stakes.length - 1;
    }
  }
}
