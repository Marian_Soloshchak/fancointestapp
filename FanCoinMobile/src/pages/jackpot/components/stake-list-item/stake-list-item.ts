import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { StakesProvider } from '../../services/stakes';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'stake-list-item',
  templateUrl: 'stake-list-item.html'
})
export class StakeListItemComponent implements OnInit, OnDestroy{

  @Input() stake: any;

  private subscriptions: Array<Subscription> = []


  constructor(private stakesService: StakesProvider) {
  }
  
  ngOnInit() {
    console.log(this.stake);
  }

  collectReward() {
    this.subscriptions.push(
      this.stakesService.collectReward(this.stake.id).subscribe(stake => {
        this.stake = stake;
      })
    )
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
