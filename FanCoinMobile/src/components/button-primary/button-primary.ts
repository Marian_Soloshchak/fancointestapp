import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'button-primary',
  templateUrl: 'button-primary.html'
})
export class ButtonPrimaryComponent {

  @Input() text: string;
  @Input() disable: boolean;
  @Output() clicked = new EventEmitter()

  constructor() {
  }

  buttonClicked(event) {
    event.stopPropagation();
    this.clicked.emit();
  }

}
