import { NgModule } from '@angular/core';
import { ButtonPrimaryComponent } from './button-primary/button-primary';
@NgModule({
	declarations: [ButtonPrimaryComponent],
	imports: [],
	exports: [ButtonPrimaryComponent]
})
export class ComponentsModule {}
